import React, { Component } from "react";
import "./App.scss";
import strategy, { solveSlow, setup } from "./solver/solver";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      grid: Array.from(Array(9).keys()).map(row =>
        Array.from(Array(9).keys()).map(number => undefined)
      )
    };
    this.rowIndex = 0;
    this.cellIndex = 0;
  }

  componentDidMount() {
    setup(state => this.setState(state));
  }

  render() {
    return (
      <main className="App">
        <header>
          <h1>Sudoku solver</h1>
          <div onClick={() => this.start()}>QuickSolve Stacc </div>
          <div onClick={() => this.iteratively()}>SlowSolve Stacc</div>
          <div onClick={() => setup(state => this.setState(state))}>Reset</div>
        </header>
        <section>
          {this.state.grid.map(row => {
            return (
              <div key={this.rowIndex++} className="row">
                {row.map(cell => {
                  const hasValue = cell && cell.value;

                  return (
                    <div
                      key={`c${this.cellIndex++}`}
                      className={`cell ${hasValue ? "hasValue" : "noValue"} ${
                        cell && cell.marked ? "marked" : ""
                      }`}
                    >
                      {hasValue ? (
                        cell.value
                      ) : cell === undefined ? (
                        ""
                      ) : (
                        <p>{cell.options.join(", ")}</p>
                      )}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </section>
      </main>
    );
  }

  start() {
    strategy(state => this.setState({ grid: state.grid }));
  }
  iteratively() {
    solveSlow(state => this.setState({ grid: state.grid }));
  }
}

export default App;
