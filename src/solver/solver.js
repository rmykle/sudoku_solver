import defaultState from "./init";
import { InitRowSolver } from "./RowSolver";
import { InitGroupSolver } from "./GroupSolver";
import { initValueChecker } from "./ValueSetter";
import { initPairSolver } from "./PairSolver";
import { initWeakPairSolver } from "./WeakPairSolver";
import { initSquareSolver } from "./SquareSolver";
import { flattenGrid } from "./util";

let grid = undefined;
let gameFinished = false;
let iterating = false;

const solveInstant = callback => {
  setup(callback);
  while (!gameFinished) {
    iterate(callback);
  }
};

export const solveSlow = callback => {
  iterating = true;
  iterateWithDelay(callback);
};
const iterateWithDelay = callback => {
  if (!gameFinished) {
    setTimeout(() => {
      if (iterating) {
        iterate(callback);
        iterateWithDelay(callback);
      }
    }, 1000);
  }
};

export const setup = callback => {
  iterating = false;
  gameFinished = false;

  grid = defaultState();
  callback({ grid: grid });
};

const iterate = callback => {
  InitRowSolver(grid);
  InitGroupSolver(grid);
  initPairSolver(grid);
  initWeakPairSolver(grid);
  initSquareSolver(grid);

  initValueChecker(grid);
  gameFinished = flattenGrid(grid).filter(cell => !cell.value).length === 0;

  callback({ grid: grid });
};

export default solveInstant;
