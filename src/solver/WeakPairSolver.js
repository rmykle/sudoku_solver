import { regroupByProperty } from "./util";

export const initWeakPairSolver = grid => {
  searchForHorizontalPairs(grid);
  searchForVerticalPairs(grid);
};

const searchForHorizontalPairs = grid => {
  for (let index in grid) {
    searchForWeakPairs(grid[index], grid);
  }
};

const searchForVerticalPairs = grid => {
  const columns = regroupByProperty(grid, "column");

  for (let index in columns) {
    searchForWeakPairs(columns[index], grid);
  }
};

const searchForWeakPairs = (list, grid) => {
  const options = list.filter(cell => {
    if (cell.options.length !== 2) return false;
    if (
      list.filter(cell2 => {
        if (cell === cell2) return false;
        if (cell.group !== cell2.group) return false;
        const cellOptionsString = cell.options.join("-");
        const cell2OptionsString = cell2.options.join("-");
        return cellOptionsString === cell2OptionsString;
      }).length === 1
    ) {
      return true;
    }
    return false;
  });

  if (options.length === 0) return;

  const groups = regroupByProperty(grid, "group");
  options.forEach(cell => {
    const cellGroup = groups.filter(filterGroup => {
      const groupID = filterGroup[0].group;
      return cell.group === groupID;
    })[0];

    const invalidOptions = [];

    for (let cellIndex in cellGroup) {
      const neighbour = cellGroup[cellIndex];
      if (neighbour.options.join("-") === cell.options.join("-")) continue;

      for (let i = 0; i < cell.options.length; i++) {
        for (let j = 0; j < neighbour.options.length; j++) {
          if (cell.options[i] === neighbour[j]) {
            invalidOptions.push(cell.options[i]);
          }
        }
      }
    }

    for (let candidateIndex in cell.options) {
      const candidate = cell.options[candidateIndex];
      if (!invalidOptions.includes(candidate)) {
        list.forEach(originalListCell => {
          if (originalListCell.options.join("-") !== cell.options.join("-")) {
            originalListCell.options = originalListCell.options.filter(
              aOption => aOption !== candidate
            );
          }
        });
      }
    }
  });
};
