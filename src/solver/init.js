const defaultState = () => {
  const assignment = assignmentGrid().map(row => {
    return row.map(cell => {
      return createNode(cell);
    });
  });

  setGroupID(assignment);
  setColumnID(assignment);
  return assignment;
};

const setGroupID = grid => {
  let rowIndex = 0;

  while (rowIndex < grid.length) {
    let cellIndex = 0;

    while (cellIndex < 9) {
      const groupID =
        rowIndex > 0
          ? Math.floor(rowIndex / 3) * 3 + Math.floor(cellIndex / 3)
          : 0 + Math.floor(cellIndex / 3);
      grid[rowIndex][cellIndex].group = groupID;

      cellIndex++;
    }

    rowIndex++;
  }
};

const setColumnID = grid => {
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[i].length; j++) {
      grid[j][i].column = i;
    }
  }
};

const assignmentGrid = () => {
  return [
    "001000000".split(""),
    "307605109".split(""),
    "050010080".split(""),
    "070403010".split(""),
    "009000500".split(""),
    "010908070".split(""),
    "040020060".split(""),
    "108506307".split(""),
    "000000000".split("")
  ];
};

const createNode = value => {
  return {
    value: value === "0" ? undefined : parseInt(value),
    options:
      value === "0" ? Array.from(Array(9).keys()).map(number => ++number) : [],
    marked: false
  };
};

export default defaultState;
