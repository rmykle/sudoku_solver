import { flattenGrid } from "./util";

export const initValueChecker = grid => {
  if (checkGroupsForSingleOption(grid)) {
    return true;
  } else if (checkHorizontalRowsForSingleOption(grid)) {
    return true;
  } else if (checkVerticalRowsForSingleOption()) {
    return true;
  } else if (checkSingleCellsForSingleOption(grid)) {
    return true;
  }
  return false;
};

const checkGroupsForSingleOption = grid => {
  for (let index in grid) {
    const group = grid[index];
    const cellUpdated = checkIfMemberIsSingleOption(group);

    if (cellUpdated) return true;
  }

  return false;
};

const checkHorizontalRowsForSingleOption = grid => {
  for (let rowIndex in grid) {
    const row = grid[rowIndex];
    const cellUpdated = checkIfMemberIsSingleOption(row);

    if (cellUpdated) return true;
  }

  return false;
};

const checkVerticalRowsForSingleOption = grid => {
  for (let index in grid) {
    const column = grid[index];
    const cellUpdated = checkIfMemberIsSingleOption(column);

    if (cellUpdated) return true;
  }

  return false;
};

const checkIfMemberIsSingleOption = list => {
  const values = {};
  list.forEach(cell => {
    cell.options.forEach(number => {
      if (values.hasOwnProperty(number)) {
        values[number] = false;
      } else {
        values[number] = true;
      }
    });
  });

  const uniqueOptions = [];
  for (let key in values) {
    if (values[key] === true) {
      uniqueOptions.push(parseInt(key));
    }
  }
  if (uniqueOptions.length === 0) return false;

  const owner = list.filter(cell => cell.options.includes(uniqueOptions[0]))[0];
  setValue(owner, uniqueOptions[0]);
  return true;
};

const checkSingleCellsForSingleOption = grid => {
  const flattenedGrid = flattenGrid(grid);
  for (let index in flattenedGrid) {
    const cell = flattenedGrid[index];
    if (cell.options.length === 1) {
      setValue(cell, cell.options[0]);
    }
  }
};

const setValue = (cell, value) => {
  cell.value = value;
  cell.options = [];
};
