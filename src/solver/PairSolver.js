import { regroupByProperty } from "./util";

export const initPairSolver = grid => {
  searchForVerticalPairs(grid);
  searchForHorizontalPairs(grid);
  searchForGroupPairs(grid);
};

const searchForHorizontalPairs = grid => {
  for (let index in grid) {
    searchForPairs(grid[index]);
  }
};
const searchForVerticalPairs = grid => {
  const columns = regroupByProperty(grid, "column");
  for (let index in columns) {
    searchForPairs(columns[index]);
  }
};

const searchForGroupPairs = grid => {
  const groups = regroupByProperty(grid, "group");
  for (let index in groups) {
    searchForPairs(groups[index]);
  }
};

const searchForPairs = list => {
  if (list[0].column === 4) {
    const candidatePairs = [];
    const options = list.reduce((currentList, current) => {
      if (current.options.length > 0) return [...currentList, current.options];
      else return currentList;
    }, []);
    const counts = {};
    for (let index in options) {
      const key = options[index].join("-");
      counts[key] = ++counts[key] || 1;
    }
    for (let optionKey in counts) {
      const candidate = optionKey.split("-");
      if (candidate.length === counts[optionKey]) {
        candidatePairs.push(candidate.map(number => parseInt(number)));
      }
    }

    for (let cellIndex in list) {
      const cell = list[cellIndex];
      if (cell.value) continue;
      for (let candidateKey in candidatePairs) {
        const candidate = candidatePairs[candidateKey];
        const { options } = cell;
        if (candidate.join("") !== options.join("")) {
          cell.options = cell.options.filter(
            number => !candidate.includes(number)
          );
        }
      }
    }
  }
};
