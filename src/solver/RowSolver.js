import { regroupByProperty, deleteCellOptionsInListUsingValues } from "./util";

export const InitRowSolver = grid => {
  updateHorizontalRowOptionsOnConcreteValues(grid);
  updateVerticalRowOptionsOnConcreteValues(grid);
};

const updateHorizontalRowOptionsOnConcreteValues = grid => {
  for (let i = 0; i < grid.length; i++) {
    const row = grid[i];
    deleteCellOptionsInListUsingValues(row);
  }
};

const updateVerticalRowOptionsOnConcreteValues = grid => {
  const rows = regroupByProperty(grid, "column");
  rows.forEach(row => {
    deleteCellOptionsInListUsingValues(row);
  });
};
