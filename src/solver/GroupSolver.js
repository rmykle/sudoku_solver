import { regroupByProperty, deleteCellOptionsInListUsingValues } from "./util";

export const InitGroupSolver = grid => {
  const groups = regroupByProperty(grid, "group");
  groups.forEach(group => deleteCellOptionsInListUsingValues(group));
};
