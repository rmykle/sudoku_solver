import { flattenGrid } from "./util";

export const initSquareSolver = grid => {
  startSquareSolver(grid);
};

const startSquareSolver = grid => {
  const optionGrid = grid.map(row => {
    return row.map(cell => cell.options);
  });

  const candidates = optionGrid.map(row => {
    const flattenedRow = flattenGrid(row);
    return row.map(optionSet =>
      optionSet.filter(
        number => flattenedRow.filter(check => check === number).length === 2
      )
    );
  });

  for (let i = 0; i < 2; i++) {
    for (let j = 0; j < candidates.length; j++) {
      const initOptions = candidates[i][j];
      for (let k = 0; k < initOptions.length; k++) {
        const candidate = initOptions[k];

        for (let l = j + 1; l < grid.length; l++) {
          if (candidates[i][l].includes(candidate)) {
            for (let m = 0; m < grid.length; m++) {
              if (
                m !== i &&
                candidates[m][j].includes(candidate) &&
                candidates[m][l].includes(candidate)
              ) {
                for (let n = 0; n < grid.length; n++) {
                  if (n !== i && n !== m) {
                    grid[n][j].options = grid[n][j].options.filter(
                      number => number !== candidate
                    );
                    grid[n][l].options = grid[n][l].options.filter(
                      number => number !== candidate
                    );
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
export default initSquareSolver;
