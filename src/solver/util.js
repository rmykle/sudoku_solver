export const flattenGrid = grid => {
  return [].concat.apply([], grid);
};

export const regroupByProperty = (grid, property) => {
  const flatten = flattenGrid(grid);

  const grouped = {};
  for (let cellIndex in flatten) {
    const cell = flatten[cellIndex];
    if (!grouped[cell[property]]) {
      grouped[cell[property]] = [];
    }
    grouped[cell[property]].push(cell);
  }
  return Object.values(grouped);
};

export const deleteCellOptionsInListUsingValues = list => {
  const values = list
    .reduce((valueList, current) => {
      return valueList.concat(current.value);
    }, [])
    .filter(option => option !== undefined);
  list.forEach(cell => {
    cell.options = cell.options.filter(option => !values.includes(option));
  });
};
